﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace Util {
    /// <summary>
    /// Represents an animation. An animation contains a list of sprites that will changed to represent movement.
    /// 
    /// The Update method should be invoked on every tick. You should use an AnimationManager to do this.
    /// </summary>
    public class Animation {
        private readonly List<Sprite> _sprites;
        private readonly List<int> _indices;
        private readonly List<double> _delays;
        private int _step;
        private double _delay;

        /// <summary>
        /// Creates the animation.
        ///
        ///This file must have a list of sprites named from 0 to 'n' and a txt named "animation.txt"
        /// containing the parameters of the animation.
        ///
        /// This animation file must have '2n' rows. Odd rows contains the next sprite index.
        /// Even rows contains the delay between the previous sprite and the next one.
        ///
        /// For example, the animation file contains this data:
        /// 0
        /// 0.1
        /// 2
        /// 0.2
        ///
        /// Will cause this behaviour:
        ///
        /// Show sprite 0.
        /// Wait 0.1 seconds.
        /// Show sprite 2.
        /// Wait 0.2 seconds.
        /// Repeat.
        /// 
        /// </summary>
        /// <param name="folder">The folder containing the animation</param>
        /// <exception cref="FormatException">Whether the animation.txt file has a bad format.</exception>
        public Animation(string folder) {
            var lines = Resources.Load<TextAsset>(folder + "/animation").text.Split('\n');
            if (lines.Length == 0) throw new FormatException("Bad data format. Empty.");
            if (lines.Length % 2 != 0)
                throw new FormatException("Bad data format. Length must be a multiple of 2. (" + lines.Length + ")");

            _sprites = GenerateSprites(folder);
            _indices = new List<int>();
            _delays = new List<double>();

            for (var i = 0; i < lines.Length; i += 2) {
                try {
                    _indices.Add(StringUtils.ToInt(lines[i]));
                    _delays.Add(double.Parse(lines[i + 1].Trim(), NumberStyles.Any, CultureInfo.InvariantCulture));
                }
                catch (Exception) {
                    Debug.LogError(lines[i]);
                    Debug.LogError(lines[i + 1]);
                    throw;
                }
            }

            _step = 0;
            _delay = _delays[0];
        }

        /// <summary>
        /// Creates the animation using a list of sprites, a list of indices and a list of delays.
        /// See the first constructor for more information.
        /// </summary>
        /// <param name="sprites">The sprites.</param>
        /// <param name="indices">The indices.</param>
        /// <param name="delays">The delays.</param>
        /// <exception cref="ArgumentException"></exception>
        public Animation(List<Sprite> sprites, List<int> indices, List<double> delays) {
            if (indices.Count == 0) throw new ArgumentException("Indices cannot be empty!");
            if (indices.Count != delays.Count)
                throw new ArgumentException("Indices and delays must have the same amount of values!");
            _sprites = sprites;
            _indices = indices;
            _delays = delays;
            _step = 0;
            _delay = delays[0];
        }

        /// <summary>
        /// Returns the list of sprites used by the animation.
        /// </summary>
        public List<Sprite> Sprites => _sprites;

        /// <summary>
        ///Returns the list of indices.
        /// </summary>
        public List<int> Indices => _indices;

        /// <summary>
        /// Returns the list of delays.
        /// </summary>
        public List<double> Delays => _delays;

        /// <summary>
        /// Returns the current step.
        /// </summary>
        public int Step {
            get => _step;
            set => _step = value;
        }

        /// <summary>
        /// Returns the current sprite.
        /// </summary>
        public Sprite Current => _sprites[_indices[_step]];

        /// <summary>
        /// Updates the animation. This method must be invoked on every tick.
        /// </summary>
        public void Update() {
            _delay -= Time.deltaTime;
            if (_delay > 0) return;

            _step++;
            if (_step == _indices.Count) _step = 0;
            _delay = _delays[_step];
        }

        private static List<Sprite> GenerateSprites(string folder) {
            folder += "/";

            var list = new List<Sprite>();

            var index = 0;
            var sprite = Resources.Load<Sprite>(folder + index++);
            while (sprite != null) {
                list.Add(sprite);
                sprite = Resources.Load<Sprite>(folder + index++);
            }

            return list;
        }
    }
}