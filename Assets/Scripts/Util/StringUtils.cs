using System.Globalization;
using System.Text;

namespace Util {
    public static class StringUtils {
        /// <summary>
        /// Unity adds some weird characters to serializable strings.
        /// This method remove those characters.
        /// </summary>
        /// <param name="s">The string to fix.</param>
        /// <returns></returns>
        public static string FixString(string s) {
            s = s.Trim();
            var builder = new StringBuilder();

            foreach (var c in s.ToCharArray()) {
                if (c >= 32 && c <= 126) builder.Append(c);
            }

            return builder.ToString();
        }


        /// <summary>
        /// Better string to int parser.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns>The int.</returns>
        public static int ToInt(string s) {
            s = s.Trim();
            if (s.Equals("0")) return 0;
            return !int.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out var result) ? 0 : result;
        }
    }
}