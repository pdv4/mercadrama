﻿using System.Collections.Generic;

namespace Util {
    /// <summary>
    /// Represents a storage for animations. This class implements an easy way to store and update animations.
    /// </summary>
    public class AnimationManager {
        private readonly Dictionary<string, Animation> _animations = new Dictionary<string, Animation>();

        /// <summary>
        /// Registers an animation.
        /// </summary>
        /// <param name="name">The animation name.</param>
        /// <param name="animation">The animation.</param>
        public void Register(string name, Animation animation) {
            _animations.Add(name, animation);
        }

        /// <summary>
        /// Try to get the animation matching the given name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="animation">The animation.</param>
        /// <returns>Whether the operation was successful.</returns>
        public bool TryGetAnimation(string name, out Animation animation) {
            return _animations.TryGetValue(name, out animation);
        }

        /// <summary>
        /// Removes the animation matching the given name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>Whether the operation was successful.</returns>
        public bool RemoveAnimation(string name) {
            return _animations.Remove(name);
        }

        /// <summary>
        /// Invokes the "Update" method of all stored animations.
        /// </summary>
        public void UpdateAnimations() {
            foreach (var entry in _animations) {
                entry.Value.Update();
            }
        }
    }
}