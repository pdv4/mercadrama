﻿namespace Util {
    public static class MusicUtils {
        
        /// <summary>
        /// Whether the music is enabled.
        /// </summary>
        public static bool Enabled { get; set; } = true;

    }
}