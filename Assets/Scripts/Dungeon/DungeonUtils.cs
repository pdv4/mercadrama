﻿using UnityEngine;

namespace Dungeon {
    public static class DungeonUtils {
        /// <summary>
        /// Returns the current SceneManager.
        /// </summary>
        /// <returns>The SceneManager</returns>
        public static SceneManager GetSceneManager() {
            return GameObject.Find("/GameManager").GetComponent<SceneManager>();
        }

        /// <summary>
        /// Changes the scene, returning to the menu.
        /// </summary>
        public static void ToMenuScene() {
            ToLevel("Scenes/Menu");
        }
        
        /// <summary>
        /// Changes the scene, returning to the menu.
        /// </summary>
        public static void ToRankingAddScene() {
            ToLevel("Scenes/RankingAdd");
        }

        /// <summary>
        /// Changes the scene.
        /// </summary>
        /// <param name="name">The new scene.</param>
        public static void ToLevel(string name) {
            UnityEngine.SceneManagement.SceneManager.LoadScene(name);
        }
    }
}