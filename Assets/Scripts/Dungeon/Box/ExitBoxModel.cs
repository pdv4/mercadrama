﻿using UnityEngine;

namespace Dungeon.Box {
    /// <summary>
    /// The model for exit boxes.
    /// </summary>
    public class ExitBoxModel : BoxModel {
        /// <summary>
        /// Creates the exit box model.
        /// </summary>
        /// <param name="view">The handled view.</param>
        public ExitBoxModel(GameObject view) : base(view, false, false, false) {
        }

        public override void RefreshSprite() {
            var renderer = View.GetComponent<SpriteRenderer>();
            renderer.sprite = Resources.Load<Sprite>("Sprites/father");
        }
    }
}