using Ranking;

namespace Dungeon.Box {
    /// <summary>
    /// Represents a exit box. This box is used to end the level.
    /// </summary>
    public class ExitBoxController : BoxController {
        /// <summary>
        /// Creates the exit box controller.
        /// </summary>
        /// <param name="model">The model to control.</param>
        public ExitBoxController(ExitBoxModel model) : base(model) {
        }

        public override void OnEnter() {
            RankingManager.AddCompletedLevel();
            DungeonUtils.ToLevel(DungeonUtils.GetSceneManager().nextLevel);
        }
    }
}