﻿using UnityEngine;

namespace Dungeon.Box {
    
    /// <summary>
    /// The default model for boxes.
    /// </summary>
    public class BoxModel {
        private readonly GameObject _view;
        private bool _collides, _isWall, _isDark;

        /// <summary>
        /// Creates the box model.
        /// </summary>
        /// <param name="view">The handled view.</param>
        /// <param name="collides">Whether this box is a collision box. Collision boxes cannot be passed through.</param>
        /// <param name="isWall">Whether this box is a wall. Walls generate shadows in the neighbour boxes.</param>
        /// <param name="isDark">Whether this box should use the dark sprites' variant.</param>
        public BoxModel(GameObject view, bool collides, bool isWall, bool isDark) {
            _view = view;
            _collides = collides;
            _isWall = isWall;
            _isDark = isDark;
        }


        /// <summary>
        /// The GameObject view of this Box.
        /// </summary>
        public GameObject View => _view;

        /// <summary>
        /// The position of the object with it's decimal part.
        /// </summary>
        public Vector3 FPosition => _view.transform.position;

        /// <summary>
        /// The integer position of the object.
        /// </summary>
        public Vector2Int Position =>
            new Vector2Int((int) _view.transform.position.x, (int) _view.transform.position.y);

        /// <summary>
        /// Whether this Box is a collision box. 
        /// </summary>
        public virtual bool Collides {
            get => _collides;
            set {
                _collides = value;
                RefreshSprite();
            }
        }

        /// <summary>
        /// Whether this box is a wall. Walls generate shadows in the neighbour boxes.
        /// </summary>
        public bool IsWall {
            get => _isWall;
            set => _isWall = value;
        }

        /// <summary>
        /// Whether this box should use the dark sprites' variant.
        /// </summary>
        public bool IsDark => _isDark;

        /// <summary>
        /// Refresh this box's sprite.
        /// </summary>
        public virtual void RefreshSprite() {
            var renderer = _view.GetComponent<SpriteRenderer>();
            var data = DungeonUtils.GetSceneManager();
            var position = _view.transform.position;
            if (_collides) {
                var left = data.GetBox(position + new Vector3(-1, 0))?.Model;
                var right = data.GetBox(position + new Vector3(1, 0))?.Model;
                var bottom = data.GetBox(position + new Vector3(0, -1))?.Model;
                var top = data.GetBox(position + new Vector3(0, 1))?.Model;

                renderer.sprite = data.CollideSprites.GetCollideSprite(
                    !bottom?._isWall ?? false,
                    !top?._isWall ?? false,
                    !left?._isWall ?? false,
                    !right?._isWall ?? false);
            }
            else {
                //Get the pass box sprite.
                var top = data.GetBox(position + new Vector3(0, 1))?.Model;
                var left = data.GetBox(position + new Vector3(-1, 0))?.Model;
                var corner = data.GetBox(position + new Vector3(-1, 1))?.Model;

                renderer.sprite = data.PassSprites.GetPassSprite(_isDark, top?._isWall ?? true,
                    left?._isWall ?? true, corner?._isWall ?? true);
            }
        }
    }
}