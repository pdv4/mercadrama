﻿using UnityEngine;

namespace Dungeon.Box {
    /// <summary>
    /// Represents a position the player can be.
    /// You can extend this class to give extra functionality to the box.
    /// </summary>
    public class BoxController {
        private readonly BoxModel _model;

        /// <summary>
        /// Creates a box.
        /// </summary>
        /// <param name="model">The box model to control.</param>
        public BoxController(BoxModel model) {
            model.View.AddComponent<SpriteRenderer>();
            _model = model;
        }

        /// <summary>
        /// The box model of this controller.
        /// </summary>
        public BoxModel Model => _model;

        /// <summary>
        /// This method is called when the players enters this Box.
        /// 
        /// This method is used by children of this class to execute
        /// custom code when the player enters the box.
        /// </summary>
        public virtual void OnEnter() {
        }
    }
}