﻿using Ranking;
using UnityEngine;

namespace Dungeon {
    public class GoMenu : MonoBehaviour {
        public void OnMouseDown() {
            RankingManager.RemoveCurrent();
            DungeonUtils.ToMenuScene();
        }
    }
}