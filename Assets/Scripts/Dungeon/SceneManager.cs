﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Dungeon.Box;
using Dungeon.Entity;
using Dungeon.Generation;
using Dungeon.Sprites;
using Ranking;
using UnityEngine;
using Util;
using Random = UnityEngine.Random;

namespace Dungeon {
    public class SceneManager : MonoBehaviour {
        [SerializeField] public int min;
        [SerializeField] public int max;
        [SerializeField] public int edge;
        [SerializeField] public string nextLevel;

        //SPRITES
        protected DungeonPassSpriteSet _passSprites;
        protected DungeonCollideSpriteSet _collideSprites;
        protected AnimationManager _animationManager;

        //BOXES
        protected BoxController[,] _boxes;
        protected Vector2Int _exit;

        //PLAYERS
        protected DungeonPlayerController _playerController;

        //ENEMIES
        protected Collection<DungeonEnemyController> _enemies;

        protected virtual void Start() {
            GenerateSpriteSets();
            CreateBoxesToGenerate(out var spawnPoint);
            GeneratePlayer(spawnPoint);
            GenerateEnemies(spawnPoint);
        }


        /// <summary>
        /// Generates the sprite sets.
        /// These instances helps to get sprites easily. 
        /// </summary>
        protected virtual void GenerateSpriteSets() {
            _animationManager = new AnimationManager();
            EntitySpriteLoader.Load(this);
            _passSprites = new DungeonPassSpriteSet();
            _collideSprites = new DungeonCollideSpriteSet();
        }

        /// <summary>
        /// Creates the BoxType array and calls GenerateBoxes.
        /// </summary>
        /// <param name="spawnPoint"></param>
        /// <exception cref="ArgumentException"></exception>
        protected virtual void CreateBoxesToGenerate(out Vector2Int spawnPoint) {
            var boxesToGenerate = DungeonGenerator.GenerateDungeon(max - min + 1, edge);
            if (max - min < 1) throw new ArgumentException("Size must be bigger than 0!");
            GenerateBoxes(boxesToGenerate, out spawnPoint);
        }


        /// <summary>
        /// Generates the boxes and some util data using a BoxType array given by CreateBoxesToGenerate.
        /// </summary>
        /// <param name="boxesToGenerate">The BoxType array.</param>
        /// <param name="spawnPoint">The SpawnPoint of the dungeon.</param>
        protected virtual void GenerateBoxes(BoxType[,] boxesToGenerate, out Vector2Int spawnPoint) {
            _boxes = new BoxController[max - min + 1, max - min + 1];
            spawnPoint = new Vector2Int();
            for (var x = min; x <= max; x++) {
                for (var y = min; y <= max; y++) {
                    //Creates the box and sets its position.
                    var o = new GameObject("Box" + x + "," + y);
                    o.transform.position = new Vector3(x, y, y / 1000f);

                    var type = boxesToGenerate[x - min, y - min];

                    //If the box is a spawn box set its position to return.
                    if (type == BoxType.Spawn) {
                        spawnPoint = new Vector2Int(x, y);
                    }

                    if (type == BoxType.Exit) {
                        _exit = new Vector2Int(x, y);
                        _boxes[x - min, y - min] = new ExitBoxController(new ExitBoxModel(o));
                    }
                    else {
                        var model = new BoxModel(o, type == BoxType.Collide,
                            type == BoxType.Collide,
                            type == BoxType.Corridor);
                        _boxes[x - min, y - min] = new BoxController(model);
                    }
                }
            }

            //Updates the sprite of all boxes.
            foreach (var box in _boxes) {
                box.Model.RefreshSprite();
            }
        }

        /// <summary>
        ///  Generates the player GameObject.
        /// </summary>
        /// <param name="spawnPoint">The player's spawn point.</param>
        protected virtual void GeneratePlayer(Vector2Int spawnPoint) {
            var playerObject = new GameObject("Player");
            var spriteRender = playerObject.AddComponent<SpriteRenderer>();
            spriteRender.sprite = Resources.Load<Sprite>("Sprites/player_debug");

            _playerController = playerObject.AddComponent<DungeonPlayerController>();

            var pos = new Vector3(spawnPoint.x, spawnPoint.y);
            pos.z = (pos.y - 0.9f) / 1000f;
            playerObject.transform.position = pos;
        }

        /// <summary>
        /// Generates all enemies.
        /// </summary>
        protected virtual void GenerateEnemies(Vector2Int playerSpawnPoint) {
            _enemies = new Collection<DungeonEnemyController>();
            foreach (var box in _boxes) {
                if (box.Model.Collides || box.Model.IsDark) continue;
                if (Random.Range(0, 20) != 0) continue;
                if (playerSpawnPoint.Equals(box.Model.Position)) continue;

                var o = new GameObject("Zombie");
                var enemy = o.AddComponent<DungeonEnemyController>();
                _enemies.Add(enemy);

                var pos = box.Model.FPosition;
                pos.z = (pos.y - 0.9f) / 1000f;
                o.transform.position = pos;
            }
        }

        /// <summary>
        /// The player handler.
        /// </summary>
        public DungeonPlayerController PlayerController => _playerController;

        /// <summary>
        /// The set that stores all pass boxes' sprites.
        /// </summary>
        public DungeonPassSpriteSet PassSprites => _passSprites;

        /// <summary>
        /// The set that stores all collide boxes' sprites.
        /// </summary>
        public DungeonCollideSpriteSet CollideSprites => _collideSprites;

        /// <summary>
        /// Returns the animation manager.
        /// </summary>
        public AnimationManager AnimationManager => _animationManager;

        /// <summary>
        /// Returns the enemies.
        /// </summary>
        public IEnumerable<DungeonEnemyController> Enemies => _enemies;

        /// <summary>
        /// Returns the exit position.
        /// </summary>
        public Vector2Int Exit => _exit;

        /// <summary>
        /// Returns the box assigned to the given position.
        /// </summary>
        /// <param name="vec">The position.</param>
        /// <returns>The box or null.</returns>
        public BoxController GetBox(Vector3 vec) {
            return GetBox((int) vec.x, (int) vec.y);
        }

        /// <summary>
        ///  Returns the box assigned to the given position.
        /// </summary>
        /// <param name="x">The X position.</param>
        /// <param name="y">The Y position.</param>
        /// <returns>The box or null.</returns>
        public BoxController GetBox(int x, int y) {
            if (x < min || y < min || x > max || y > max) return null;
            return _boxes[x - min, y - min];
        }

        /// <summary>
        ///  Sets a box to the given position.
        /// </summary>
        /// <param name="x">The X position.</param>
        /// <param name="y">The Y position.</param>
        /// <param name="boxController">The box to set.</param>
        /// <returns>The box or null.</returns>
        public void SetBox(int x, int y, BoxController boxController) {
            if (x < min || y < min || x > max || y > max) return;
            _boxes[x - min, y - min] = boxController;
        }

        /// <summary>
        /// Stops the game. This is called when the player dies.
        /// </summary>
        public void GameOver() {
            if (RankingManager.Stop()) {
                DungeonUtils.ToRankingAddScene();
            }
            else {
                DungeonUtils.ToMenuScene();
            }
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                DungeonUtils.ToMenuScene();
            }

            //Updates animations.
            _animationManager.UpdateAnimations();
        }
    }
}