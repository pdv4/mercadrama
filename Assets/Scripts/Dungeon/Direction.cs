namespace Dungeon {
    /// <summary>
    /// Represents a direction. Used by the player.
    /// </summary>
    public enum Direction {
        Up,
        Down,
        Left,
        Right
    }
}