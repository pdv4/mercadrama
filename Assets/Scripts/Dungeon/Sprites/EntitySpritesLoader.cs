using Util;

namespace Dungeon.Sprites {
    public static class EntitySpriteLoader {
        
        /// <summary>
        /// Loads all entities' animations.
        /// </summary>
        /// <param name="sceneManager">The scene manager handling the dungeon.</param>
        public static void Load(SceneManager sceneManager) {
            const string playerMainFolder = "Sprites/Player/";
            const string enemyMainFolder = "Sprites/Zombie/";
            var manager = sceneManager.AnimationManager;

            manager.Register("player_down", new Animation(playerMainFolder + "Down"));
            manager.Register("player_left", new Animation(playerMainFolder + "Left"));
            manager.Register("player_right", new Animation(playerMainFolder + "Right"));
            manager.Register("player_up", new Animation(playerMainFolder + "Up"));

            manager.Register("enemy_down", new Animation(enemyMainFolder + "Down"));
            manager.Register("enemy_left", new Animation(enemyMainFolder + "Left"));
            manager.Register("enemy_right", new Animation(enemyMainFolder + "Right"));
            manager.Register("enemy_up", new Animation(enemyMainFolder + "Up"));
        }
    }
}