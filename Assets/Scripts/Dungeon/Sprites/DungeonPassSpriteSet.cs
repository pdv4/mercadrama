using UnityEngine;

namespace Dungeon.Sprites {
    /// <summary>
    /// The instance of this class stores all pass boxes' sprites.
    /// You can use the method GetPassSprite to get the sprite assigned
    /// to the given booleans.
    /// </summary>
    public class DungeonPassSpriteSet {
        //darkPassSprite are for those paths that doesnt have an open end 
        //lightPassSprites are the floor for open spaces 
        private readonly Sprite[] _darkPassSprites, _lightPassSprites;


        /// <summary>
        /// Creates the dungeon pass sprite set.
        /// </summary>
        public DungeonPassSpriteSet() {
            GeneratePassSprites(_darkPassSprites = new Sprite[5], true);
            GeneratePassSprites(_lightPassSprites = new Sprite[5], false);
        }


        /// <summary>
        /// Returns the sprite assigned to the given variables.
        /// </summary>
        /// <param name="dark">Whether the sprite should be of the dark variable.</param>
        /// <param name="hasTopWall">Whether the box has a wall on top of it.</param>
        /// <param name="hasLeftWall">Whether the box has a wall at the left.</param>
        /// <param name="hasCornerShadow">Whether the bos has a wall at the top left.</param>
        /// <returns>The sprite.</returns>
        public Sprite GetPassSprite(bool dark, bool hasTopWall, bool hasLeftWall, bool hasCornerShadow) {
            var array = dark ? _darkPassSprites : _lightPassSprites;
            //var index = (hasTopWall ? 2 : 0) + (hasLeftWall ? 1 : 0);
            //if (index == 0 && hasCornerShadow) index = 4;
            return array[0];
        }


        private static void GeneratePassSprites(Sprite[] array, bool dark) {
            //if is dark uses the dark_floor sprite, else uses light_floor one
            var prefix = dark ? "Sprites/Box/dark_floor" : "Sprites/Box/light_floor";
            array[0] = Resources.Load<Sprite>(prefix);
            array[1] = Resources.Load<Sprite>(prefix + "_with_left_shadow");
            array[2] = Resources.Load<Sprite>(prefix + "_with_top_shadow");
            array[3] = Resources.Load<Sprite>(prefix + "_with_top_left_shadow");
            array[4] = Resources.Load<Sprite>(prefix + "_with_corner_shadow");
        }
    }
}