using System.Text;
using UnityEngine;

namespace Dungeon.Sprites {
    /// <summary>
    /// The instance of this class stores all collide boxes' sprites.
    /// You can use the method GetCollideSprite to get the sprite assigned
    /// to the given booleans.
    /// </summary>
    public class DungeonCollideSpriteSet {
        private Sprite[,] _walls;


        /// <summary>
        /// Creates the dungeon collide sprite set.
        /// </summary>
        public DungeonCollideSpriteSet() {
            _walls = new Sprite[3, 3];

            for (var y = -1; y < 2; y++) {
                for (var x = -1; x < 2; x++) {
                    if (y == 0 && x == 0) {
                        _walls[1, 1] = Resources.Load<Sprite>("Sprites/Box/wall");
                        continue;
                    }

                    var builder = new StringBuilder();
                    if (y == -1) builder.Append("bottom_");
                    else if (y == 1) builder.Append("top_");
                    if (x == -1) builder.Append("left_");
                    else if (x == 1) builder.Append("right_");
                    builder.Append("shelf");

                    var sprite = Resources.Load<Sprite>("Sprites/Box/Shelf/" + builder);
                    _walls[x + 1, y + 1] = sprite;
                }
            }
        }
        
        /// <summary>
        /// Returns the collide sprite.
        /// </summary>
        /// <param name="down">Whether the down box is not a wall.</param>
        /// <param name="up">Whether the up box is not a wall.</param>
        /// <param name="left">Whether the left box is not a wall.</param>
        /// <param name="right">Whether the right box is not a wall.</param>
        /// <returns></returns>
        public Sprite GetCollideSprite(bool down, bool up, bool left, bool right) {

            int x = 0, y = 0;
            if (down) y = -1;
            else if (up) y = 1;
            if (right) x = 1;
            else if (left) x = -1;

            return _walls[x + 1, y + 1];
        }
    }
}