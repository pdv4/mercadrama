﻿using UnityEngine;

namespace Dungeon.Arrow {
    
    /// <summary>
    /// Represents the arrow's controller.
    /// </summary>
    public class ArrowController : MonoBehaviour {
        private ArrowModel _model;
        private SceneManager _sceneManager;

        private void Start() {
            _model = new ArrowModel(gameObject);
        }

        private void Update() {
            if (_sceneManager == null) {
                _sceneManager = DungeonUtils.GetSceneManager();
                if (_sceneManager == null) return;
            }

            var from = _sceneManager.PlayerController.Model.Position;
            var to = _sceneManager.Exit;
            _model.PointTo(from, to);
        }

        
        /// <summary>
        /// The arrow's model.
        /// </summary>
        public ArrowModel Model => _model;
    }
}