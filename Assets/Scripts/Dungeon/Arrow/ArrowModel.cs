﻿using System;
using UnityEngine;

namespace Dungeon.Arrow {
    /// <summary>
    /// Represents the arrow's model.
    /// </summary>
    public class ArrowModel {
        private readonly GameObject _view;

        /// <summary>
        /// Creates the arrow's model.
        /// </summary>
        /// <param name="view">The arrow's view to handle.</param>
        public ArrowModel(GameObject view) {
            _view = view;
        }

        /// <summary>
        /// The arrow's view handled by this model.
        /// </summary>
        public GameObject View => _view;


        /// <summary>
        /// Points the arrow to the given position 'to' from the given position 'from'.
        /// </summary>
        /// <param name="from">The point where the arrow point.</param>
        /// <param name="to">The point where the arrow points at.</param>
        public void PointTo(Vector2Int from, Vector2Int to) {
            //var direction = new Vector3(to.x - from.x, to.y - from.y).normalized;
            var angle = (float) (Math.Atan2(to.y - from.y, to.x - from.x) * 180 / Math.PI);
            _view.transform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}