﻿using System;
using Ranking;
using UnityEngine;

namespace Dungeon.Countdown {
    /// <summary>
    /// Represents the countdown's controller.
    /// </summary>
    public class CountdownController : MonoBehaviour {
        /// <summary>
        /// The time on the first level.
        /// </summary>
        private const int DefaultTime = 180;

        /// <summary>
        /// The minimum amount of time the player must have each level.
        /// </summary>
        private const int MinTime = 40;

        /// <summary>
        /// Every level this value is subtracted from the start time.
        /// </summary>
        private const int TimeRemovedPerLevel = 20;


        private CountdownModel _model;
        private float _maxTime;

        private void Start() {
            _model = new CountdownModel(gameObject);
            var levels = RankingManager.CompletedLevels;
            _maxTime = Time.time + Math.Max(MinTime, DefaultTime - levels * TimeRemovedPerLevel);
        }

        private void Update() {
            var now = Time.time;
            if (_maxTime <= now) DungeonUtils.GetSceneManager().GameOver();

            _model.SetTime(_maxTime - now);
        }
    }
}