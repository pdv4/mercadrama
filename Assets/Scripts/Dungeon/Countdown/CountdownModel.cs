﻿using TMPro;
using UnityEngine;

namespace Dungeon.Countdown {
    /// <summary>
    /// Represents the countdown's model.
    /// </summary>
    public class CountdownModel {
        private readonly GameObject _view;
        private readonly TextMeshProUGUI _text;

        /// <summary>
        /// Creates the countdown's model.
        /// </summary>
        /// <param name="view">The countdown's view to handle.</param>
        public CountdownModel(GameObject view) {
            _view = view;
            _text = view.GetComponent<TextMeshProUGUI>();
        }

        /// <summary>
        /// Returns the handled view.
        /// </summary>
        public GameObject View => _view;

        /// <summary>
        /// Sets the given time on the countdown.
        /// </summary>
        /// <param name="fSeconds">the time in seconds.</param>
        public void SetTime(float fSeconds) {
            var total = (int) (fSeconds * 1000);
            var millis = total % 1000;
            total /= 1000;
            var seconds = total % 60;
            total /= 60;
            _text.text = total + ":" + AddZeros(seconds, 2) + ":" + AddZeros(millis, 3);
        }


        private static string AddZeros(int time, int length) {
            var str = time.ToString();
            while (str.Length < length) {
                str = "0" + str;
            }

            return str;
        }
    }
}