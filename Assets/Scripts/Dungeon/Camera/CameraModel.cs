﻿using Dungeon.Entity;
using UnityEngine;

namespace Dungeon.Camera {
    /// <summary>
    /// The model for a moving camera.
    /// </summary>
    public class CameraModel {
        private readonly GameObject _view;

        private Vector3 _to;

        /// <summary>
        /// Creates the model.
        /// </summary>
        /// <param name="view">The handled view.</param>
        public CameraModel(GameObject view) {
            _view = view;
        }

        /// <summary>
        /// The view handled by this model.
        /// </summary>
        public GameObject View => _view;

        /// <summary>
        /// The final position of the camera.
        /// This position is where the camera is moving to.
        /// </summary>
        public Vector3 To {
            get => _to;
            set {
                _to = value;
                _to.z = -1;
            }
        }

        /// <summary>
        /// The current position of the camera.
        /// </summary>
        public Vector3 Position {
            get => _view.transform.position;
            set => _view.transform.position = value;
        }

        /// <summary>
        /// This setter triggers the smooth movement method.
        /// </summary>
        public Vector3 SmoothPosition {
            set => ApplySmoothPosition(value);
        }

        private void ApplySmoothPosition(Vector3 currentPosition) {
            if (currentPosition == _to) return;
            //Smooth movement.
            var u = _to - currentPosition;
            var newPosition = currentPosition + u * (Time.deltaTime / (DungeonEntityController.Delay * 1.5f));
            var limitMovement = Vector3.Dot(_to - currentPosition, _to - newPosition) < 0;
            _view.transform.position = limitMovement ? _to : newPosition;
        }
    }
}