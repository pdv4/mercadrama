﻿using UnityEngine;

namespace Dungeon.Camera {
    /// <summary>
    /// Makes the camera move along the player.
    /// </summary>
    public class CameraController : MonoBehaviour {
        private GameObject _player;
        private CameraModel _model;

        private void Start() {
            _model = new CameraModel(gameObject);
        }

        private void Update() {
            if (_player == null) {
                _player = DungeonUtils.GetSceneManager().PlayerController.gameObject;
                _model.Position = _player.transform.position;
            }

            _model.To = _player.transform.position;

            //Refreshes the smooth position.
            _model.SmoothPosition = _model.Position;
        }
    }
}