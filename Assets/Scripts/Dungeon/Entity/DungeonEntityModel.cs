﻿using UnityEngine;

namespace Dungeon.Entity {
    
    /// <summary>
    /// The base mode for an entity.
    /// </summary>
    public class DungeonEntityModel {
        private readonly GameObject _view;

        private Vector3 _to;
        private Direction _direction;
        private bool _moving;

        /// <summary>
        /// Creates the model.
        /// </summary>
        /// <param name="view">The handled view.</param>
        public DungeonEntityModel(GameObject view) {
            _view = view;

            _to = view.transform.position;
            _moving = false;
            _direction = Direction.Down;
        }

        /// <summary>
        /// The handled view.
        /// </summary>
        public GameObject View => _view;

        /// <summary>
        /// The position where this entity is moving to.
        /// </summary>
        public Vector3 To {
            get => _to;
            set => _to = value;
        }

        /// <summary>
        /// Returns the position of the entity.
        /// </summary>
        public Vector2Int Position => new Vector2Int((int) _to.x, (int) _to.y);

        /// <summary>
        /// The decimal position of the entity.
        /// Updating this property triggers the ApplySmoothMovement method.
        /// </summary>
        public Vector3 FPosition {
            get => _view.transform.position;
            set => ApplySmoothMovement(value);
        }

        /// <summary>
        /// Returns the direction os the entity.
        /// </summary>
        public Direction Direction {
            get => _direction;
            set => _direction = value;
        }

        /// <summary>
        /// Returns whether the entity is moving.
        /// </summary>
        public bool Moving {
            get => _moving;
            set => _moving = value;
        }


        /// <summary>
        /// Updates the player sprite. Used every time the sprite should be changed.
        /// </summary>
        public virtual void UpdateSprite(SceneManager sceneManager) {
        }

        /// <summary>
        /// Smoothly moves the entity to the next box.
        /// This method should be called on every frame to work properly.
        /// </summary>
        private void ApplySmoothMovement(Vector3 currentPosition) {
            var u = (_to - currentPosition).normalized;
            var newPosition = currentPosition + u * (Time.deltaTime / (DungeonEntityController.Delay + 0.02f));
            var limitMovement = Vector3.Dot(_to - currentPosition, _to - newPosition) < 0;
            newPosition.z = (newPosition.y - 0.9f) / 1000f;
            _view.transform.position = limitMovement ? _to : newPosition;
        }
    }
}