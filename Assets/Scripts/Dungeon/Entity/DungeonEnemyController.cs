﻿using System.Linq;
using Dungeon.Box;
using UnityEngine;

namespace Dungeon.Entity {
    /// <summary>
    /// Manages an enemy's movement.
    /// </summary>
    public class DungeonEnemyController : DungeonEntityController {
        private Direction _selectedDirection;

        protected override void Start() {
            base.Start();
            _selectedDirection = GetRandomDirection();
        }

        protected override void Update() {
            base.Update();
            if (_delay <= 0) return;
            _delay -= Time.deltaTime;
            if (_delay <= 0) Model.Moving = false;
            Model.UpdateSprite(_sceneManager);
        }

        /// <summary>
        /// Executed when a player moves. This method moves the enemy one step, or rotates it.
        /// </summary>
        public void ExecuteTurn() {
            if (!Move(_selectedDirection, out _)) {
                _selectedDirection = GetRandomDirection();
            }
            else if (_sceneManager.PlayerController.Model.Position.Equals(Model.Position)) {
                _sceneManager.GameOver();
            }
        }

        public override bool CanMoveTo(Vector3Int position, BoxController to) {
            if (!base.CanMoveTo(position, to)) return false;

            if (new Vector2Int(position.x, position.y).Equals(_sceneManager.Exit)) return false;

            return _sceneManager.Enemies
                .All(enemy => !enemy.Model.Position.Equals(new Vector2Int(position.x, position.y)));
        }

        protected override DungeonEntityModel GenerateModel(GameObject view) {
            return new DungeonEnemyModel(view);
        }

        /// <summary>
        /// Returns a random direction.
        /// </summary>
        /// <returns>The direction.</returns>
        private static Direction GetRandomDirection() {
            return (Direction) Random.Range(0, 4);
        }
    }
}