﻿using System.Collections.Generic;
using System.Linq;
using Dungeon.Box;
using UnityEngine;

namespace Dungeon.Entity {
    /// <summary>
    /// Manages a player's movement.
    /// </summary>
    public class DungeonPlayerController : DungeonEntityController {
        /// <summary>
        /// Keys that are listened by the player.
        /// </summary>
        private static readonly KeyCode[] Keys = {KeyCode.W, KeyCode.A, KeyCode.S, KeyCode.D};

        /// <summary>
        /// Currently pressed keys.
        /// </summary>
        private readonly LinkedList<KeyCode> _keys = new LinkedList<KeyCode>();

        protected override void Update() {
            base.Update();

            //Gets the pressed keys.
            foreach (var keyCode in Keys) {
                if (Input.GetKeyDown(keyCode))
                    _keys.AddFirst(keyCode);
                if (Input.GetKeyUp(keyCode))
                    _keys.Remove(keyCode);
            }


            _delay -= Time.deltaTime;

            //If the delay > 0 or there are no pressed keys, return.
            if (_delay > 0) {
                Model.UpdateSprite(_sceneManager);
                return;
            }

            if (_keys.Count == 0) {
                if (!Model.Moving) return;
                Model.Moving = false;
                Model.UpdateSprite(_sceneManager);
                return;
            }


            //Gets the last pressed key and adds the offset.
            var direction = Direction.Down;
            switch (_keys.First.Value) {
                case KeyCode.W:
                    direction = Direction.Up;
                    break;
                case KeyCode.S:
                    direction = Direction.Down;
                    break;
                case KeyCode.A:
                    direction = Direction.Left;
                    break;
                case KeyCode.D:
                    direction = Direction.Right;
                    break;
            }

            if (!Move(direction, out var box)) return;
            box.OnEnter();
            foreach (var enemy in _sceneManager.Enemies) {
                enemy.ExecuteTurn();
            }

            if (!CanPlayerMove()) _sceneManager.GameOver();
        }

        public override bool CanMoveTo(Vector3Int position, BoxController to) {
            if (!base.CanMoveTo(position, to)) return false;

            return _sceneManager.Enemies
                .All(enemy => !enemy.Model.Position.Equals(new Vector2Int(position.x, position.y)));
        }

        protected override DungeonEntityModel GenerateModel(GameObject view) {
            return new DungeonPlayerModel(view);
        }

        /// <summary>
        /// Returns whether the player can move to any position.
        /// </summary>
        /// <returns>Whether the player can move to any position</returns>
        private bool CanPlayerMove() {
            const int directions = 4;
            for (var i = 0; i < directions; i++) {
                var direction = (Direction) i;
                var position = Model.To + GetRelativeByDirection(direction);
                var box = _sceneManager.GetBox(position);
                if (CanMoveTo(new Vector3Int((int) position.x, (int) position.y, (int) position.z), box))
                    return true;
            }

            return false;
        }
    }
}