﻿using UnityEngine;

namespace Dungeon.Entity {
    
    /// <summary>
    /// The model for enemies.
    /// </summary>
    public class DungeonEnemyModel : DungeonEntityModel {
        
        /// <summary>
        /// Creates the model.
        /// </summary>
        /// <param name="view">The handled view.</param>
        public DungeonEnemyModel(GameObject view) : base(view) {
        }


        public override void UpdateSprite(SceneManager sceneManager) {
            var spriteRenderer = View.GetComponent<SpriteRenderer>();
            var name = "enemy_" + Direction.ToString().ToLower();
            if (!sceneManager.AnimationManager.TryGetAnimation(name, out var animation))
                return;
            spriteRenderer.sprite = !Moving ? animation.Sprites[0] : animation.Current;
        }
    }
}