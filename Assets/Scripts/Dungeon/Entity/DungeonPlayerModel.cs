﻿using UnityEngine;

namespace Dungeon.Entity {
    /// <summary>
    /// The model for the player.
    /// </summary>
    public class DungeonPlayerModel : DungeonEnemyModel {
        /// <summary>
        /// Creates the player's model.
        /// </summary>
        /// <param name="view">The player's view to handle.</param>
        public DungeonPlayerModel(GameObject view) : base(view) {
        }


        public override void UpdateSprite(SceneManager sceneManager) {
            var spriteRenderer = View.GetComponent<SpriteRenderer>();
            var name = "player_" + Direction.ToString().ToLower();
            if (!sceneManager.AnimationManager.TryGetAnimation(name, out var animation))
                return;

            if (Moving) {
                spriteRenderer.sprite = animation.Current;
            }
            else if (Direction == Direction.Down || Direction == Direction.Up) {
                spriteRenderer.sprite = animation.Sprites[0];
            }
            else {
                spriteRenderer.sprite = animation.Sprites[2];
            }
        }
    }
}