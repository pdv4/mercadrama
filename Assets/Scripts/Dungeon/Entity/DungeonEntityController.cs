﻿using Dungeon.Box;
using UnityEngine;

namespace Dungeon.Entity {
    
    /// <summary>
    /// The base controller for an entity.
    /// Extends this to create new entities.
    /// </summary>
    public class DungeonEntityController : MonoBehaviour {
        public const float Delay = 0.1f;

        private DungeonEntityModel _model;

        protected float _delay;
        protected SceneManager _sceneManager;

        protected virtual void Start() {
            var gObject = gameObject;
            gObject.AddComponent<SpriteRenderer>();
            _model = GenerateModel(gObject);

            _delay = 0;
            _sceneManager = DungeonUtils.GetSceneManager();
            _model.UpdateSprite(_sceneManager);
        }

        protected virtual void Update() {
            //Applies smooth movement.
            _model.FPosition = _model.FPosition;
        }

        public DungeonEntityModel Model => _model;

        /// <summary>
        /// Moves the entity one step, if possible.
        /// </summary>
        /// <param name="direction">The direction where the entity is moving.</param>
        /// <param name="toBoxController">The next box.</param>
        /// <returns>Whether the entity was moved.</returns>
        protected bool Move(Direction direction, out BoxController toBoxController) {
            var to = _model.To;
            _model.Direction = direction;

            to += GetRelativeByDirection(direction);

            toBoxController = _sceneManager.GetBox(to);

            if (!CanMoveTo(new Vector3Int((int) to.x, (int) to.y, (int) to.z), toBoxController)) {
                _model.Moving = false;
                _model.UpdateSprite(_sceneManager);
                return false;
            }

            _delay = Delay;
            to.z = (to.y - 0.9f) / 1000f;

            _model.To = to;
            _model.Moving = true;

            _model.UpdateSprite(_sceneManager);
            return true;
        }

        /// <summary>
        /// Called to check if an entity can move to the given position.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="to">The box.</param>
        /// <returns>Whether the entity can move.</returns>
        public virtual bool CanMoveTo(Vector3Int position, BoxController to) {
            return to != null && !to.Model.Collides;
        }


        protected virtual DungeonEntityModel GenerateModel(GameObject view) {
            return new DungeonEntityModel(view);
        }

        /// <summary>
        /// Returns the relative movement a Direction should execute.
        /// </summary>
        /// <param name="direction">The direction.</param>
        /// <returns>The relative movement.</returns>
        protected static Vector3 GetRelativeByDirection(Direction direction) {
            switch (direction) {
                case Direction.Up:
                    return new Vector3(0, 1);
                case Direction.Down:
                    return new Vector3(0, -1);
                case Direction.Left:
                    return new Vector3(-1, 0);
                case Direction.Right:
                    return new Vector3(1, 0);
            }

            return new Vector3();
        }
    }
}