using System.Collections.Generic;
using System.Collections.ObjectModel;
using Dungeon.Entity;
using Dungeon.Generation;
using UnityEngine;
using Util;

namespace Dungeon {
    public class FileSceneManager : SceneManager {
        [SerializeField] public string level;

        private Dictionary<Vector2Int, string> enemiesToGenerate;

        protected override void CreateBoxesToGenerate(out Vector2Int spawnPoint) {
            var boxesToGenerate = DungeonFileGenerator.GenerateDungeon(StringUtils.FixString(level),
                out enemiesToGenerate);
            min = 0;
            max = boxesToGenerate.GetLength(0) - 1;
            GenerateBoxes(boxesToGenerate, out spawnPoint);
        }


        protected override void GenerateEnemies(Vector2Int playerSpawnPoint) {
            _enemies = new Collection<DungeonEnemyController>();
            foreach (var pair in enemiesToGenerate) {
                if (pair.Value.Equals("zombie")) {
                    var o = new GameObject("Zombie");
                    var enemy = o.AddComponent<DungeonEnemyController>();
                    _enemies.Add(enemy);

                    var pos = new Vector3(pair.Key.x - min, pair.Key.y - min);
                    pos.z = (pos.y - 0.9f) / 1000f;
                    o.transform.position = pos;
                }
            }
        }
    }
}