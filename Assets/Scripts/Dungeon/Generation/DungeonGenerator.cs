using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Dungeon.Generation {
    public static class DungeonGenerator {
        /// <summary>
        /// Generates the dungeon using a Tunneling Algorithm.
        /// This algorithm creates similar dungeons as the dungeons
        /// in Pokémon Mystery Dungeon.
        /// 
        /// Use it as a dark-box method.
        /// </summary>
        /// <param name="length">The length of the dungeon.</param>
        /// <param name="edge">The collision blocks on the edges.</param>
        /// <returns>The BoxType array.</returns>
        public static BoxType[,] GenerateDungeon(int length, int edge) {
            var array = new BoxType[length, length];

            var rooms = new List<Room>();
            var roomAmount = Random.Range(6, 12);
            for (var i = 0; i < roomAmount; i++) {
                if (!GenerateRoom(array, edge, out var room)) continue;
                rooms.Add(room);

                if (rooms.Count == 1) continue;
                var prev = rooms[rooms.Count - 2];

                room.GetRandomPosition(out var toX, out var toY);
                prev.GetRandomPosition(out var fromX, out var fromY);

                if (Random.Range(0f, 1f) > 0.5) {
                    GenerateHorizontalTunnel(array, edge, fromX, toX, fromY);
                    GenerateVerticalTunnel(array, edge, fromY, toY, toX);
                }
                else {
                    GenerateVerticalTunnel(array, edge, fromY, toY, fromX);
                    GenerateHorizontalTunnel(array, edge, fromX, toX, toY);
                }
            }


            var boxCells = new List<Vector2Int>();


            for (var x = 0; x < length; x++) {
                for (var y = 0; y < length; y++) {
                    var box = array[x, y];
                    if (box == BoxType.Room) boxCells.Add(new Vector2Int(x, y));
                }
            }

            var spawnPointIndex = Random.Range(0, boxCells.Count - 1);
            var spawnPoint = boxCells[spawnPointIndex];
            array[spawnPoint.x, spawnPoint.y] = BoxType.Spawn;
            boxCells.RemoveAt(spawnPointIndex);

            boxCells.Sort((o1, o2) => 
                Math.Abs(o2.x - spawnPoint.x) + Math.Abs(o2.y - spawnPoint.x) - Math.Abs(o1.x - spawnPoint.x) - Math.Abs(o1.y - spawnPoint.x));
            
            var exit = boxCells[0];
            array[exit.x, exit.y] = BoxType.Exit;

            return array;
        }

        private static bool GenerateRoom(BoxType[,] array, int edge, out Room room) {
            var length = array.GetLength(0);
            var xPos = Random.Range(3 + edge, length - 3 - edge);
            var yPos = Random.Range(3 + edge, length - 3 - edge);
            var width = Random.Range(7, 12);
            var height = Random.Range(7, 12);


            width -= Math.Max((xPos + width) - length, 0);
            height -= Math.Max((yPos + height) - length, 0);

            room = new Room(xPos, yPos, width, height);

            for (var x = 0; x < width; x++) {
                for (var y = 0; y < height; y++) {
                    var finalX = xPos + x;
                    var finalY = yPos + y;

                    if (finalX - edge < 0 || finalX + edge > length) continue;
                    if (finalY - edge < 0 || finalY + edge > length) continue;

                    array[finalX, finalY] = BoxType.Room;
                }
            }

            return true;
        }

        private static void GenerateHorizontalTunnel(BoxType[,] array, int edge, int fromX, int toX, int y) {
            var min = Math.Min(fromX, toX) - 1;
            var max = Math.Max(fromX, toX) + 1;
            var length = array.GetLength(0);
            for (var x = min; x <= max; x++) {
                for (var iy = y - 1; iy < y + 2; iy++) {
                    if (x >= length || x < 0 || iy >= length || iy < 0) continue;
                    var value = array[x, iy];
                    if (value == BoxType.Collide) {
                        if (x - edge < 0 || x + edge > length) continue;
                        if (iy - edge < 0 || iy + edge > length) continue;

                        array[x, iy] = BoxType.Corridor;
                    }
                }
            }
        }

        private static void GenerateVerticalTunnel(BoxType[,] array, int edge, int fromY, int toY, int x) {
            var min = Math.Min(fromY, toY) + 1;
            var max = Math.Max(fromY, toY) - 1;
            var length = array.GetLength(0);
            for (var y = min; y <= max; y++) {
                for (var ix = x - 1; ix < x + 2; ix++) {
                    if (ix >= length || ix < 0 || y >= length || y < 0) continue;
                    var value = array[ix, y];
                    if (value == BoxType.Collide) {
                        if (ix - edge < 0 || ix + edge > length) continue;
                        if (y - edge < 0 || y + edge > length) continue;

                        array[ix, y] = BoxType.Corridor;
                    }
                }
            }
        }

        private class Room {
            private int _x, _y, _width, _height;

            public Room(int x = default, int y = default, int width = default, int height = default) {
                _x = x;
                _y = y;
                _width = width;
                _height = height;
            }

            public void GetRandomPosition(out int x, out int y) {
                x = Random.Range(_x, _x + _width - 1);
                y = Random.Range(_y, _y + _height - 1);
            }

            public bool Intersects(Room o) {
                return _x <= o._x + o._width &&
                       _x + _width >= o._x &&
                       _y <= o._y + o._height &&
                       _y + _height >= o._y;
            }
        }
    }
}