using System.Collections.Generic;
using UnityEngine;

namespace Dungeon.Generation {
    public static class DungeonFileGenerator {
        /// <summary>
        /// Generates a dungeon using the given file.
        /// This parses the file, getting the dungeon's data from it.
        /// </summary>
        /// <param name="fileName">The name of the file</param>
        /// <param name="enemies">The generated enemies</param>
        /// <returns>The BoxType array.</returns>
        public static BoxType[,] GenerateDungeon(string fileName, out Dictionary<Vector2Int, string> enemies) {
            var lines = Resources.Load<TextAsset>(fileName).text.Split('\n');

            string line;
            var index = 0;
            while (index < lines.Length && !(line = lines[index].Trim()).ToLower().Equals("start")) {
                Debug.Log(line);
                //TODO CUSTOM BOXES
                index++;
            }

            if (index >= lines.Length) {
                enemies = null;
                return null;
            }

            //Gets the width and height of the dungeon.
            var width = int.Parse(lines[++index]);
            var height = int.Parse(lines[++index]);

            var array = new BoxType[width, height];

            //Creates enemies dictionary
            enemies = new Dictionary<Vector2Int, string>();

            //Parses all lines.
            index++;
            var y = height - 1;
            while (index < lines.Length) {
                if (y == 0) break;
                line = lines[index];

                var x = 0;

                foreach (var box in line.Split('\t')) {
                    var sl = box.Split('-');
                    if (sl.Length > 1) {
                        switch (sl[1].ToLower()) {
                            case "z":
                                enemies.Add(new Vector2Int(x, y), "zombie");
                                break;
                        }
                    }

                    array[x, y] = Parse(sl[0]);
                    x++;
                    if (x == width) break;
                }

                index++;
                y--;
            }

            return array;
        }


        /// <summary>
        /// Parses a character into a BoxType.
        /// </summary>
        /// <param name="box">The box to parse.</param>
        /// <returns>The parsed BoxType</returns>
        private static BoxType Parse(string box) {
            switch (box.ToLower()) {
                case "c":
                    return BoxType.Collide;
                case "r":
                    return BoxType.Room;
                case "cr":
                    return BoxType.Corridor;
                case "e":
                    return BoxType.Exit;
                case "s":
                    return BoxType.Spawn;
                default:
                    return BoxType.Collide;
            }
        }
    }
}