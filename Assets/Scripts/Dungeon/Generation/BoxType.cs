namespace Dungeon.Generation {
    /// <summary>
    /// This class is used by the dungeons generates to
    /// store the types of every generated box.
    /// </summary>
    public enum BoxType {
        Collide,
        Room,
        Corridor,
        Exit,
        Spawn
    }
}