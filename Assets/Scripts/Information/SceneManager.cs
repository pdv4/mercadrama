﻿using Ranking;
using UnityEngine;
using UnityEngine.UI;

namespace Information {
    public class SceneManager : MonoBehaviour {
        public Text info;
        public string path = "Text/explanation";
        public int page;

        private void Start() {
            info.text = "";
            page = 1; 
        }

        private void Update() {
            info.text = RankingUtils.ReadText(path);
        }

        private void OnMouseDown() {
            switch (page) {
                case 1:
                    path = "Text/explanation2";
                    page = 2;
                    break;
                case 2:
                    path = "Text/explanation";
                    page = 1;
                    break;
            }
        }
    }
}