﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;
using Util;

namespace Ranking {
    public static class RankingManager {
        private const string FILE_PATH = "ranking.dat";

        /// <summary>
        /// The ranking.
        /// </summary>
        private static readonly List<RankingEntry> _times = new List<RankingEntry>();

        /// <summary>
        /// The current chronometer.
        /// </summary>
        private static float _currentStartTimestamp;

        /// <summary>
        /// The time the player has spent in the game.
        /// Calculated at the end.
        /// </summary>
        private static float _totalTime;

        /// <summary>
        /// The amount of completed levels.
        /// </summary>
        private static int _completedLevels;

        /// <summary>
        /// Whether the chronometer is running.
        /// </summary>
        private static bool _running;

        /// <summary>
        /// Whether the ranking has been init.
        /// </summary>
        private static bool _init;

        /// <summary>
        /// The ranking.
        /// </summary>
        public static List<RankingEntry> Times => _times;

        /// <summary>
        /// The completed levels of this run.
        /// </summary>
        public static int CompletedLevels => _completedLevels;

        /// <summary>
        /// Starts the chronometer, if not running.
        /// </summary>
        public static void Start() {
            if (_running) return;
            _currentStartTimestamp = Time.time;
            _completedLevels = 0;

            _running = true;
        }

        /// <summary>
        /// Stops the chronometer and stores an entry, if running.
        /// <returns>Whether the time should be saved.</returns>
        /// </summary>
        public static bool Stop() {
            if (!_running) return false;
            _totalTime = Time.time - _currentStartTimestamp;
            _running = false;
            return _completedLevels > 0;
        }

        public static void AddRanking(string name) {
            if (_running) return;
            var time2 = Math.Round(_totalTime, 2);
            _times.Add(new RankingEntry(name, time2, _completedLevels));
            //Sorting
            Sort();
            Save();
        }

        /// <summary>
        /// Stops the current chronometer without saving an entry.
        /// </summary>
        public static void RemoveCurrent() {
            _running = false;
        }

        /// <summary>
        /// Adds a completed level to the counter.
        /// </summary>
        public static void AddCompletedLevel() {
            _completedLevels++;
        }

        /// <summary>
        /// Transforms the ranking into a string.
        /// </summary>
        /// <returns>The string.</returns>
        public static string RankingToString() {
            const string header = "Top 5";
            var builder = new StringBuilder(header);
            builder.Append("\n");

            var i = 0;

            while (i < 5 && i < _times.Count) {
                builder.Append(_times[i]);
                if (i != 4 && i != _times.Count - 1)
                    builder.Append("\n");
                i++;
            }

            return builder.ToString();
        }

        public static void Init() {
            if (_init) return;
            Load();
            _init = true;
        }

        private static void Load() {
            if (!File.Exists(FILE_PATH)) return;
            var stream = new StreamReader(File.OpenRead(FILE_PATH));
            var amount = int.Parse(stream.ReadLine(), NumberStyles.Float, CultureInfo.InvariantCulture);

            for (var i = 0; i < amount; i++) {
                var name = stream.ReadLine();
                var time = double.Parse(stream.ReadLine(), NumberStyles.Float, CultureInfo.InvariantCulture);
                var levels = StringUtils.ToInt(stream.ReadLine());
                _times.Add(new RankingEntry(name, time, levels));
            }

            Sort();
            stream.Close();
        }

        private static void Save() {
            var stream = new StreamWriter(File.Open(FILE_PATH, FileMode.Create, FileAccess.Write));
            stream.WriteLine(_times.Count);
            foreach (var entry in _times) {
                stream.WriteLine(entry.Name);
                stream.WriteLine(entry.Time);
                stream.WriteLine(entry.Levels);
            }

            stream.Close();
        }

        private static void Sort() {
            _times.Sort((o1, o2) => o1.CompareTo(o2));
        }
    }
}