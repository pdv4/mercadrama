﻿using UnityEngine;

namespace Ranking {
    public static class RankingUtils {
        public static string ReadText(string path) {
            Debug.Log("READING PATH " + path);
            return Resources.Load<TextAsset>(path).text;
        }
    }
}