﻿using UnityEngine;
using UnityEngine.UI;

namespace Ranking {
    public class SceneManager : MonoBehaviour {
        public Text score;
        public string path = "Text/score";

        private void Start() {
            score.text = RankingManager.RankingToString();
        }
    }
}