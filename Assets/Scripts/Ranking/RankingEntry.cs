﻿namespace Ranking {
    /// <summary>
    /// Represents a entry in the ranking.
    /// </summary>
    public class RankingEntry {
        /// <summary>
        /// Creates a ranking entry.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="time">The time</param>
        /// <param name="levels">The amount of levels the player has completed</param>
        public RankingEntry(string name, double time, int levels) {
            Name = name;
            Time = time;
            Levels = levels;
        }

        /// <summary>
        /// Returns the name of the entry.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Returns the time of the entry.
        /// </summary>
        public double Time { get; }

        /// <summary>
        /// Returns the amount of levels the player has completed.
        /// </summary>
        public int Levels { get; }

        public override string ToString() {
            return Name + " " + Time + " (Lv: " + Levels + ")";
        }

        /// <summary>
        /// Compares this entry with with the given one.
        /// </summary>
        /// <param name="entry">The given entry.</param>
        /// <returns>Less than 0 if this entry has priority,
        /// more than 0 if the given entry has priority or 0 if both have the same priority.</returns>
        public int CompareTo(RankingEntry entry) {
            if (entry.Levels == Levels) return Time.CompareTo(entry.Time);
            return entry.Levels - Levels;
        }
    }
}