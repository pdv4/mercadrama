﻿using System;
using System.Collections.Generic;
using Dungeon;
using Ranking;
using UnityEngine;
using UnityEngine.UI;

namespace RankingAdd {
    public class TextScript : MonoBehaviour {
        public Text _name;

        public void AddName() {
            RankingManager.AddRanking(_name.text);
            DungeonUtils.ToMenuScene();
        }
    }
}