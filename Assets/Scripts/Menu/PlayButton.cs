﻿using Ranking;
using UnityEngine;

namespace Menu {
    public class PlayButton : MonoBehaviour {
        private void OnMouseOver() {
            if (!Input.GetKey(KeyCode.Mouse0)) return;
            RankingManager.Start();
            MenuUtils.ChangeScene("Scenes/Level");
        }
    }
}