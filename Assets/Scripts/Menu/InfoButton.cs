﻿using UnityEngine;

namespace Menu {
    public class InfoButton : MonoBehaviour {
        private void OnMouseOver() {
            if (Input.GetKey(KeyCode.Mouse0)) {
                MenuUtils.ChangeScene("Scenes/Information");
            }
        }
    }
}