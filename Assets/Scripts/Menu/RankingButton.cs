﻿using UnityEngine;

namespace Menu {
    public class RankingButton : MonoBehaviour {
        private void OnMouseOver() {
            if (Input.GetKey(KeyCode.Mouse0)) {
                MenuUtils.ChangeScene("Scenes/Ranking");
            }
        }
    }
}