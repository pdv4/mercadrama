﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu {
    public static class MenuUtils {
        public static void ChangeScene(string scene) {
            UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
        }

        public static void InsertSprite(GameObject gameObject, string path) {
            gameObject.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(path);
        }
    }
}