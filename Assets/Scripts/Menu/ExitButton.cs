﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Menu {
    public class ExitButton : MonoBehaviour {
        private void OnMouseOver() {
            if (Input.GetKey(KeyCode.Mouse0)) {
                MenuUtils.ChangeScene("Scenes/Menu");
            }
        }
    }
}