﻿using Ranking;
using UnityEngine;

namespace Menu {
    public class SceneManager : MonoBehaviour {
        private void Start() {
            RankingManager.Init();
            //If it's the first time the game loads, this method will do nothing.
            RankingManager.Stop();
        }

        private void Update() {
            if (Input.GetKey(KeyCode.Escape)) {
                Application.Quit(0);
            }
        }
    }
}