﻿using UnityEngine;
using Util;

namespace Music {
    /// <summary>
    /// The controller for the music button.
    /// </summary>
    public class MusicButtonController : MonoBehaviour {
        [SerializeField] public string song;

        private MusicButtonModel _model;

        private void Start() {
            Debug.Log(song);
            _model = new MusicButtonModel(gameObject, song);
        }

        private void OnMouseOver() {
            if (!Input.GetKeyDown(KeyCode.Mouse0)) return;
            MusicUtils.Enabled = !MusicUtils.Enabled;
            _model.RefreshAudio();
            _model.RefreshSprite();
        }
    }
}