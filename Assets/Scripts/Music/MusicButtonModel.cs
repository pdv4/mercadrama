﻿using UnityEngine;
using Util;

namespace Music {
    /// <summary>
    /// The model for the music button.
    /// </summary>
    public class MusicButtonModel {
        private readonly GameObject _view;

        private readonly SpriteRenderer _renderer;
        private readonly AudioSource _source;

        /// <summary>
        /// Creates the model for the music button.
        /// </summary>
        /// <param name="view">The view to handle.</param>
        /// <param name="song">The song to play.</param>
        public MusicButtonModel(GameObject view, string song) {
            _view = view;

            _renderer = _view.AddComponent<SpriteRenderer>();
            _source = _view.AddComponent<AudioSource>();

            _source.clip = Resources.Load<AudioClip>(song);
            _source.loop = true;
            _source.Play();
            RefreshSprite();
            RefreshAudio();
        }

        /// <summary>
        /// The handled view.
        /// </summary>
        public GameObject View => _view;

        /// <summary>
        /// Refreshes the sprite of the button.
        /// </summary>
        public void RefreshSprite() {
            var spriteName = MusicUtils.Enabled ? "Sprites/Sound/sound_on" : "Sprites/Sound/sound_off";
            _renderer.sprite = Resources.Load<Sprite>(spriteName);
        }

        /// <summary>
        /// Refreshes the audio status.
        /// </summary>
        public void RefreshAudio() {
            if (MusicUtils.Enabled) {
                _source.UnPause();
            }
            else {
                _source.Pause();
            }
        }
    }
}